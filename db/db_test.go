package db

import (
	"bitbucket.org/pvasilenka/juno-test/db/node"
	"context"
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
	"time"
)

// BenchmarkStorage_Get is for personal usage to check concurrent operations
// And approximately see performance
func BenchmarkStorage_Get(b *testing.B) {
	st := NewStorage(context.Background())
	var wg sync.WaitGroup
	for n := 0; n < b.N; n++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			st.Set("test", node.NewStringNode("test", 1*time.Second))
		}()
		wg.Add(1)
		go func() {
			defer wg.Done()
			st.Set("test2", node.NewListNode([]string{"test"}, 2*time.Second))
		}()
		wg.Add(1)
		go func() {
			defer wg.Done()
			st.Set("test3", node.NewDictNode(map[string]string{"test": "test2"}, 1*time.Second))
		}()
		wg.Add(1)
		go func() {
			defer wg.Done()
			st.Update("test3", node.NewStringNode("test", 3*time.Second))
		}()
		wg.Add(1)
		go func() {
			defer wg.Done()
			st.Update("test", node.NewListNode([]string{"test"}, 5*time.Second))
		}()
		wg.Add(1)
		go func() {
			defer wg.Done()
			st.Update("test2", node.NewDictNode(map[string]string{"test": "test2"}, 5*time.Second))
		}()
		wg.Add(1)
		go func() {
			defer wg.Done()
			st.Get("test")
		}()
		wg.Add(1)
		go func() {
			defer wg.Done()
			st.Get("test2")
		}()
		wg.Add(1)
		go func() {
			defer wg.Done()
			st.Get("test3")
		}()
		wg.Add(1)
		go func() {
			defer wg.Done()
			st.Delete("test")
		}()
		wg.Add(1)
		go func() {
			defer wg.Done()
			st.Delete("test2")
		}()
		wg.Add(1)
		go func() {
			defer wg.Done()
			st.Delete("test3")
		}()
		wg.Wait()
	}
}

func TestSchema_Set_Get(t *testing.T) {
	st := NewStorage(context.Background())

	i1 := node.NewDictNode(map[string]string{"test": "test2"}, 0)
	i2 := node.NewStringNode("test", 0)
	i3 := node.NewListNode([]string{"test"}, 0)

	st.Set("test", i1)
	st.Set("test2", i2)
	st.Set("test3", i3)

	val1, err := st.Get("test")
	assert.Equal(t, val1, i1)
	assert.NoError(t, err)

	val2, err := st.Get("test2")
	assert.Equal(t, val2, i2)
	assert.NoError(t, err)

	val3, err := st.Get("test3")
	assert.Equal(t, val3, i3)
	assert.NoError(t, err)

	assert.Equal(t, 3, st.ItemCount())

	st.Delete("test3")
	assert.Equal(t, 2, st.ItemCount())

	_, err = st.Get("test3")
	assert.Error(t, err)
}

func TestSchema_Set_Expired(t *testing.T) {
	st := NewStorage(context.Background())
	i := node.NewDictNode(map[string]string{"test": "test2"}, 1*time.Second)
	st.Set("test", i)
	time.Sleep(1001 * time.Millisecond)
	_, err := st.Get("test")
	assert.Error(t, err)
}

func TestSchema_Set_NotExpired(t *testing.T) {
	st := NewStorage(context.Background())
	i := node.NewDictNode(map[string]string{"test": "test2"}, 2*time.Second)
	st.Set("test", i)

	time.Sleep(1 * time.Second)
	n, err := st.Get("test")
	assert.NoError(t, err)
	assert.Equal(t, i, n)

	time.Sleep(1001 * time.Millisecond)
	_, err = st.Get("test")
	assert.Error(t, err)
}
