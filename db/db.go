package db

import (
	"bitbucket.org/pvasilenka/juno-test/db/node"
	"bitbucket.org/pvasilenka/juno-test/expirer"
	"bitbucket.org/pvasilenka/juno-test/internal/escape"
	"context"
	"errors"
	"sync"
	"time"
)

// ErrNotFound is returned when there is not row by key in database
var ErrNotFound = errors.New("row not found")

// item is internal wrapper over Node
// That is actually isn't really useful for now
type item struct {
	n node.Node
}

// Storage represents inmem storage
// That might be used separately or in the scope of bucket-based DB
type Storage struct {
	d   map[string]item
	l   sync.RWMutex
	e   expirer.KeyExpirer
	ctx context.Context
	c   int
}

// NewStorage initializes storage and starts expiring goroutine
func NewStorage(ctx context.Context) *Storage {
	s := &Storage{
		d:   make(map[string]item),
		e:   expirer.NewPQExpirer(ctx),
		ctx: ctx,
	}

	go s.listenExpired()

	return s
}

// Set sets item to database and pushes key to expirer
func (s *Storage) Set(key string, n node.Node) {
	s.l.Lock()
	defer s.l.Unlock()

	s.set(key, n)
}

// Get item by key
// Will return ErrNotFound in case there is no item in storage or it is expired
func (s *Storage) Get(key string) (node.Node, error) {
	s.l.RLock()
	defer s.l.RUnlock()

	return s.get(key)
}

// Update updates item by key
// It also resets TTL to what is set in item
func (s *Storage) Update(key string, n node.Node) error {
	s.l.Lock()
	defer s.l.Unlock()

	if _, err := s.get(key); err != nil {
		return err
	}

	s.set(key, n)

	return nil
}

// ItemCount returns amount of items in storage
func (s *Storage) ItemCount() int {
	s.l.RLock()
	defer s.l.RUnlock()

	return s.c
}

// Keys returns list of keys in storage
func (s *Storage) Keys() []string {
	s.l.RLock()
	defer s.l.RUnlock()

	return s.keys()
}

// Delete deletes item from db
func (s *Storage) Delete(key string) error {
	s.l.Lock()
	defer s.l.Unlock()
	return s.delete(key)
}

// GetElementFromListByIndex returns element of array
func (s *Storage) GetElementFromListByIndex(key string, ind int) (node.Node, error) {
	s.l.RLock()
	defer s.l.RUnlock()

	n, err := s.get(key)
	if err != nil {
		return nil, err
	}

	l, ok := n.GetData().([]string)
	if !ok {
		return nil, errors.New("value stored by key is not list")
	}

	if ind >= len(l) {
		return nil, errors.New("index out of array bounds")
	}

	return node.NewStringNode(l[ind], 0), nil
}

// SetElementToListByIndex sets element to list by index
func (s *Storage) SetElementToListByIndex(key string, ind int, elem string) error {
	s.l.RLock()
	defer s.l.RUnlock()

	n, err := s.get(key)
	if err != nil {
		return err
	}

	l, ok := n.GetData().([]string)
	if !ok {
		return errors.New("value stored by key is not list")
	}

	if ind >= len(l) {
		return errors.New("index out of array bounds")
	}

	l[ind] = elem

	return nil
}

// GetElementFromListByIndex returns element of dict which is stored by key
func (s *Storage) GetElementFromDictByKey(key, dkey string) (node.Node, error) {
	s.l.RLock()
	defer s.l.RUnlock()

	n, err := s.get(key)
	if err != nil {
		return nil, err
	}

	l, ok := n.GetData().(map[string]string)
	if !ok {
		return nil, errors.New("value stored by key is not dict")
	}

	if d, ok := l[dkey]; !ok {
		return nil, errors.New("there is no such key in dict")
	} else {
		return node.NewStringNode(d, 0), nil
	}
}

// SetElementToDictByKey sets element to dict
func (s *Storage) SetElementToDictByKey(key, dkey, item string) error {
	s.l.RLock()
	defer s.l.RUnlock()

	n, err := s.get(key)
	if err != nil {
		return err
	}

	l, ok := n.GetData().(map[string]string)
	if !ok {
		return errors.New("value stored by key is not dict")
	}

	l[dkey] = item

	return nil
}

func (s *Storage) keys() []string {
	ks := make([]string, len(s.d))

	i := 0
	for k := range s.d {
		ks[i] = escape.Whitespace(k)
		i++
	}

	return ks
}

func (s *Storage) set(key string, n node.Node) {
	// we assume mutex is already locked
	s.d[key] = item{n: n}
	s.c++

	if !n.GetExpiredAt().IsZero() {
		s.e.Expire(key, n.GetExpiredAt())
	}
}

func (s *Storage) delete(key string) error {
	// we don't use get because it will return ErrNotFound in case if row is expired
	// we want to delete expired rows without this check
	if _, ok := s.d[key]; !ok {
		return ErrNotFound
	}

	delete(s.d, key)
	s.c--

	return nil
}

func (s *Storage) get(key string) (node.Node, error) {
	// We assume mutex is already locked
	if n, ok := s.d[key]; !ok {
		return nil, ErrNotFound
		// We're checking for expiry time to prevent concurrency issues
		// causing some items don't expire by exact time
	} else if !n.n.GetExpiredAt().IsZero() && n.n.GetExpiredAt().Before(time.Now()) {
		return nil, ErrNotFound
	} else {
		return n.n, nil
	}
}

// expire checks if key in database, checks it expiry date and expires it accordingly
func (s *Storage) expire(key string) {
	s.l.Lock()
	defer s.l.Unlock()
	// If no error that means item is not expired because it is updated, so we don't need to delete it
	// A little bit later we will receive expiring message for update operation
	if _, err := s.get(key); err == nil {
		return
	}

	delete(s.d, key)
	s.c--
}

// listenExpired is a function which listens to expired items channel
func (s *Storage) listenExpired() {
	for {
		select {
		case ex := <-s.e.Expired():
			s.expire(ex.GetKey())
		case <-s.ctx.Done():
			return
		}
	}
}
