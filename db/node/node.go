package node

import (
	"bitbucket.org/pvasilenka/juno-test/internal/escape"
	"errors"
	"strings"
	"time"
)

// Node is interface which represents struct to be stored in storage
type Node interface {
	GetData() interface{}
	GetExpiredAt() time.Time
	GetTTL() time.Duration
	String() string
	GetType() string
}

// GetNodeFromString accepts string representation of node and generates new node based on this
func GetNodeFromString(t string, v []string, ttl time.Duration) (Node, error) {
	t = strings.ToLower(t)
	switch t {
	case "list":
		return NewListNode(v, ttl), nil
	case "dict":
		val := v

		if len(val)%2 != 0 {
			return nil, errors.New("data should be formatted as <key> <value>")
		}

		dict := make(map[string]string)

		for i := 0; i < len(val); i += 2 {
			dict[val[i]] = val[i+1]
		}

		return NewDictNode(dict, ttl), nil
	case "string":
		return NewStringNode(v[0], ttl), nil
	default:
		return nil, errors.New("bad node type")
	}
}

// StringNode is a node which stores string
type StringNode struct {
	val       string
	expiredAt time.Time
	ttl       time.Duration
}

// NewStringNode creates new node
func NewStringNode(val string, ttl time.Duration) *StringNode {
	sn := &StringNode{val: val, ttl: ttl}
	if ttl > 0 {
		sn.expiredAt = time.Now().Add(ttl)
	}
	return sn
}

// GetData returns string data stored in node
func (sn *StringNode) GetData() interface{} {
	return sn.val
}

// GetExpiredAt returns expiry date of node
func (sn *StringNode) GetExpiredAt() time.Time {
	return sn.expiredAt
}

// GetTTL returns TTL defined on node creation
func (sn *StringNode) GetTTL() time.Duration {
	return sn.ttl
}

// GetType returns node type
func (sn *StringNode) GetType() string {
	return "string"
}

// String returns string representation of node
func (sn *StringNode) String() string {
	return sn.GetType() + " " + escape.Whitespace(sn.val)
}

// ListNode contains list of values
type ListNode struct {
	val       []string
	expiredAt time.Time
	ttl       time.Duration
}

// NewListNode creates node containing list of values
func NewListNode(val []string, ttl time.Duration) *ListNode {
	ln := &ListNode{val: val, ttl: ttl}
	if ttl > 0 {
		ln.expiredAt = time.Now().Add(ttl)
	}
	return ln
}

// GetData returns data stored in node
func (ln *ListNode) GetData() interface{} {
	return ln.val
}

// GetExpiredAt returns time when to expire node
func (ln *ListNode) GetExpiredAt() time.Time {
	return ln.expiredAt
}

func (ln *ListNode) GetTTL() time.Duration {
	return ln.ttl
}

// GetType returns node type
func (ln *ListNode) GetType() string {
	return "list"
}

// String returns string representation of node
func (ln *ListNode) String() string {
	str := ""

	for i, v := range ln.val {
		str += escape.Whitespace(v)
		if i != len(ln.val)-1 {
			str += " "
		}
	}

	return ln.GetType() + " " + str
}

// DictNode contains key => value based data
type DictNode struct {
	val       map[string]string
	expiredAt time.Time
	ttl       time.Duration
}

// NewDictNode creates new dict node
func NewDictNode(val map[string]string, ttl time.Duration) *DictNode {
	dn := &DictNode{val: val, ttl: ttl}
	if ttl > 0 {
		dn.expiredAt = time.Now().Add(ttl)
	}
	return dn
}

// GetData returns data stored in node
func (dn *DictNode) GetData() interface{} {
	return dn.val
}

// GetExpiredAt returns time when to expire node
func (dn *DictNode) GetExpiredAt() time.Time {
	return dn.expiredAt
}

// GetTTL returns TTL defined on node creation
func (dn *DictNode) GetTTL() time.Duration {
	return dn.ttl
}

// GetType returns node type
func (dn *DictNode) GetType() string {
	return "dict"
}

// String returns string representation of node
func (dn *DictNode) String() string {
	var l []string

	c := 0
	for k, v := range dn.val {
		l = append(l, escape.Whitespace(k)+" "+escape.Whitespace(v))
		c++
	}

	return dn.GetType() + " " + strings.Join(l, " ")
}
