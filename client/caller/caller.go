package caller

import (
	"bitbucket.org/pvasilenka/juno-test/db/node"
	"bitbucket.org/pvasilenka/juno-test/internal/escape"
	"bitbucket.org/pvasilenka/juno-test/internal/reader"
	"context"
	"errors"
	"fmt"
	"github.com/reiver/go-oi"
	"github.com/reiver/go-telnet"
	"strconv"
	"sync"
)

// List of command patterns used to generate command string
const (
	GetCommandPattern        = "GET %s\r\n"
	SetCommandPattern        = "SET %s %d %s\r\n"
	UpdateCommandPattern     = "UPDATE %s %d %s\r\n"
	DeleteCommandPattern     = "DELETE %s\r\n"
	CountCommandPattern      = "COUNT\r\n"
	KeysCommandPattern       = "KEYS\r\n"
	QuitCommandPattern       = "QUIT\r\n"
	AuthCommandPattern       = "AUTH %s %s\r\n"
	DictGetKeyCommandPattern = "DGETK %s %s\r\n"
	DictSetKeyCommandPattern = "DSETK %s %s %s\r\n"
	ListGetCommandPattern    = "LGET %s %d\r\n"
	ListSetCommandPattern    = "LSET %s %d %s\r\n"
)

// List of response statuses might be received from server
const (
	ErrorResponseStatus = "ERROR"
	OKResponseStatus    = "OK"
)

// AuthConfig is a configuration to authenticate onto calling server
type AuthConfig struct {
	Username string
	Password string
}

// ClientCaller is caller through TELNET library
type ClientCaller struct {
	w  telnet.Writer
	r  telnet.Reader
	tr *reader.TelnetReader

	m   sync.Mutex
	ctx context.Context
	a   *AuthConfig
}

// NewClientCaller returns caller to client
func NewClientCaller(ctx context.Context, a *AuthConfig) *ClientCaller {
	c := &ClientCaller{ctx: ctx, a: a}
	// Lock until connection is established and served
	c.m.Lock()

	return c
}

// CallTELNET is function to communicate with TELNET server
func (c *ClientCaller) CallTELNET(ctx telnet.Context, w telnet.Writer, r telnet.Reader) {
	c.w = w
	c.r = r
	c.tr = reader.NewTelnetReader(c.r)
	// Skip HELLO message
	c.tr.ReadLine()

	if c.a != nil {
		c.auth(c.a.Username, c.a.Password)
	}

	// Unlock mutex as connection is established
	c.m.Unlock()
	// Waiting until application is closed
	<-c.ctx.Done()
}

// Set will send set command to server
func (c *ClientCaller) Set(key string, n node.Node) error {
	c.m.Lock()
	defer c.m.Unlock()

	oi.LongWriteString(c.w, fmt.Sprintf(SetCommandPattern, key, int(n.GetTTL().Seconds()), n.String()))

	t, err := c.tr.ReadLine()
	if err != nil {
		return err
	}

	if string(t) != ErrorResponseStatus {
		return nil
	}

	t, err = c.tr.ReadLine()
	if err != nil {
		return err
	}

	return errors.New(string(t))
}

// Get will send get command to server and return node representation of data
func (c *ClientCaller) Get(key string) (node.Node, error) {
	c.m.Lock()
	defer c.m.Unlock()

	oi.LongWriteString(c.w, fmt.Sprintf(GetCommandPattern, key))

	line, err := c.tr.ReadLine()
	if err != nil {
		return nil, err
	}

	t := string(line)
	if t != OKResponseStatus {
		line, err := c.tr.ReadLine()
		if err != nil {
			return nil, err
		}

		return nil, errors.New(string(line))
	}

	line, err = c.tr.ReadLine()
	if err != nil {
		return nil, err
	}

	args := escape.ParseString(string(line))

	return node.GetNodeFromString(args[0], args[1:], 0)
}

// Update will send update command to server
func (c *ClientCaller) Update(key string, n node.Node) error {
	c.m.Lock()
	defer c.m.Unlock()

	oi.LongWriteString(c.w, fmt.Sprintf(UpdateCommandPattern, key, int(n.GetTTL().Seconds()), n.String()))

	t, err := c.tr.ReadLine()
	if err != nil {
		return err
	}

	if string(t) != ErrorResponseStatus {
		return nil
	}

	t, err = c.tr.ReadLine()
	if err != nil {
		return err
	}

	return errors.New(string(t))
}

// Delete will send delete command to server
func (c *ClientCaller) Delete(key string) error {
	c.m.Lock()
	defer c.m.Unlock()

	oi.LongWriteString(c.w, fmt.Sprintf(DeleteCommandPattern, key))

	t, err := c.tr.ReadLine()
	if err != nil {
		return err
	}

	if string(t) != ErrorResponseStatus {
		return nil
	}

	t, err = c.tr.ReadLine()
	if err != nil {
		return err
	}

	return errors.New(string(t))
}

// DGetK return value from dict by dict key
func (c *ClientCaller) DGetK(key, dkey string) (node.Node, error) {
	c.m.Lock()
	defer c.m.Unlock()

	oi.LongWriteString(c.w, fmt.Sprintf(DictGetKeyCommandPattern, key, dkey))

	line, err := c.tr.ReadLine()
	if err != nil {
		return nil, err
	}

	t := string(line)
	if t != OKResponseStatus {
		line, err := c.tr.ReadLine()
		if err != nil {
			return nil, err
		}

		return nil, errors.New(string(line))
	}

	line, err = c.tr.ReadLine()
	if err != nil {
		return nil, err
	}

	args := escape.ParseString(string(line))

	return node.GetNodeFromString(args[0], args[1:], 0)
}

// DSetK will send dsetk command to server
func (c *ClientCaller) DSetK(key, dkey, value string) error {
	c.m.Lock()
	defer c.m.Unlock()

	oi.LongWriteString(c.w, fmt.Sprintf(DictSetKeyCommandPattern, key, dkey, value))

	t, err := c.tr.ReadLine()
	if err != nil {
		return err
	}

	if string(t) != ErrorResponseStatus {
		return nil
	}

	t, err = c.tr.ReadLine()
	if err != nil {
		return err
	}

	return errors.New(string(t))
}

// DGetK return value from dict by dict key
func (c *ClientCaller) LGet(key string, index int) (node.Node, error) {
	c.m.Lock()
	defer c.m.Unlock()

	oi.LongWriteString(c.w, fmt.Sprintf(ListGetCommandPattern, key, index))

	line, err := c.tr.ReadLine()
	if err != nil {
		return nil, err
	}

	t := string(line)
	if t != OKResponseStatus {
		line, err := c.tr.ReadLine()
		if err != nil {
			return nil, err
		}

		return nil, errors.New(string(line))
	}

	line, err = c.tr.ReadLine()
	if err != nil {
		return nil, err
	}

	args := escape.ParseString(string(line))

	return node.GetNodeFromString(args[0], args[1:], 0)
}

// LSet will send dsetk command to server
func (c *ClientCaller) LSet(key string, index int, value string) error {
	c.m.Lock()
	defer c.m.Unlock()

	oi.LongWriteString(c.w, fmt.Sprintf(ListSetCommandPattern, key, index, value))

	t, err := c.tr.ReadLine()
	if err != nil {
		return err
	}

	if string(t) != ErrorResponseStatus {
		return nil
	}

	t, err = c.tr.ReadLine()
	if err != nil {
		return err
	}

	return errors.New(string(t))
}

// ItemCount will send Count command to server and return int value
func (c *ClientCaller) ItemCount() (int, error) {
	c.m.Lock()
	defer c.m.Unlock()

	oi.LongWriteString(c.w, CountCommandPattern)

	line, err := c.tr.ReadLine()
	if err != nil {
		return 0, err
	}

	t := string(line)
	if t != OKResponseStatus {
		line, err := c.tr.ReadLine()
		if err != nil {
			return 0, err
		}

		return 0, errors.New(string(line))
	}

	line, err = c.tr.ReadLine()
	if err != nil {
		return 0, err
	}

	res, err := strconv.Atoi(string(line))
	if err != nil {
		return 0, err
	}

	return res, nil
}

// Keys will send keys command to server and return list of keys
func (c *ClientCaller) Keys() ([]string, error) {
	c.m.Lock()
	defer c.m.Unlock()

	oi.LongWriteString(c.w, KeysCommandPattern)

	line, err := c.tr.ReadLine()
	if err != nil {
		return nil, err
	}

	t := string(line)
	if t != OKResponseStatus {
		line, err := c.tr.ReadLine()
		if err != nil {
			return nil, err
		}

		return nil, errors.New(string(line))
	}

	line, err = c.tr.ReadLine()
	if err != nil {
		return nil, err
	}

	return escape.ParseString(string(line)), nil
}

// auth authenticates on server
func (c *ClientCaller) auth(un, pwd string) error {
	// assuming mutex is locked
	oi.LongWriteString(c.w, fmt.Sprintf(AuthCommandPattern, un, pwd))

	t, err := c.tr.ReadLine()
	if err != nil {
		return err
	}

	if string(t) != ErrorResponseStatus {
		return nil
	}

	t, err = c.tr.ReadLine()
	if err != nil {
		return err
	}

	return errors.New(string(t))
}

// Close quits from TELNET session with server
func (c *ClientCaller) Close() {
	c.m.Lock()
	defer c.m.Unlock()

	oi.LongWriteString(c.w, QuitCommandPattern)
}
