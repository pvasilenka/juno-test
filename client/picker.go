package client

import (
	"github.com/pkg/errors"
	"github.com/serialx/hashring"
	"strconv"
	"sync"
)

// Picker is an interface to pick appropriate server to store keys
type Picker interface {
	PickClient(key string) (StorageClient, error)
	GetAll() []StorageClient
}

// StorageClientPicker is a struct which picks storage client to store value by key
// This client is thread safe
type StorageClientPicker struct {
	hr    *hashring.HashRing
	nodes map[string]StorageClient

	// Mutex is used to sync hashring operations, which are not thread safe
	m sync.Mutex
}

// NewStoragePicker instantiates storage picker
func NewStorageClientPicker(s ...StorageClient) *StorageClientPicker {
	scp := &StorageClientPicker{
		nodes: make(map[string]StorageClient),
	}

	servers := make([]string, len(s))
	for i, sc := range s {
		key := strconv.Itoa(i)
		servers[i] = key
		scp.nodes[key] = sc
	}

	hr := hashring.New(servers[:])
	scp.hr = hr

	return scp
}

// PickClient picks appropriate client in set
func (scp *StorageClientPicker) PickClient(key string) (StorageClient, error) {
	scp.m.Lock()
	defer scp.m.Unlock()

	// Boolean parameter which says `true` if node is found.
	// It returns false only in case if there is no servers at all
	s, ok := scp.hr.GetNode(key)
	if !ok {
		return nil, errors.New("there is no servers set")
	}

	return scp.nodes[s], nil
}

// GetAll returns all clients
func (scp *StorageClientPicker) GetAll() []StorageClient {
	var sc []StorageClient
	for _, v := range scp.nodes {
		sc = append(sc, v)
	}

	return sc
}
