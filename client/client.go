package client

import (
	"bitbucket.org/pvasilenka/juno-test/db/node"
)

// StorageClient is a communicator which should be used to communicate with server itself
type StorageClient interface {
	Set(key string, n node.Node) error
	Get(key string) (node.Node, error)
	Update(key string, n node.Node) error
	ItemCount() (int, error)
	Keys() ([]string, error)
	Delete(key string) error
	DGetK(key, dkey string) (node.Node, error)
	DSetK(key, dkey, value string) error
	LGet(key string, index int) (node.Node, error)
	LSet(key string, index int, value string) error
}

// Client represents client and it's communication with StorageClient
type Client struct {
	s []StorageClient
	p Picker
}

// NewClient returns client
func NewClient(sc ...StorageClient) *Client {
	return &Client{s: sc, p: NewStorageClientPicker(sc...)}
}

// Set is used to set node by key
func (c *Client) Set(key string, n node.Node) error {
	s, err := c.p.PickClient(key)
	if err != nil {
		return err
	}
	return s.Set(key, n)
}

// Get is used to get node by key
func (c *Client) Get(key string) (node.Node, error) {
	s, err := c.p.PickClient(key)
	if err != nil {
		return nil, err
	}
	return s.Get(key)
}

// Update is used to update node by key
// It also resets TTL to passed
func (c *Client) Update(key string, n node.Node) error {
	s, err := c.p.PickClient(key)
	if err != nil {
		return err
	}
	return s.Update(key, n)
}

// Delete removes node from storage by key
func (c *Client) Delete(key string) error {
	s, err := c.p.PickClient(key)
	if err != nil {
		return err
	}
	return s.Delete(key)
}

// DGetK gets item by dkey from dict stored by key
func (c *Client) DGetK(key, dkey string) (node.Node, error) {
	s, err := c.p.PickClient(key)
	if err != nil {
		return nil, err
	}
	return s.DGetK(key, dkey)
}

// DSetK sets item by dkey to dict stored by key
func (c *Client) DSetK(key, dkey, value string) error {
	s, err := c.p.PickClient(key)
	if err != nil {
		return err
	}
	return s.DSetK(key, dkey, value)
}

// LGet gets item by index from list stored by key
func (c *Client) LGet(key string, index int) (node.Node, error) {
	s, err := c.p.PickClient(key)
	if err != nil {
		return nil, err
	}
	return s.LGet(key, index)
}

// LSet sets item by index to list stored by key
func (c *Client) LSet(key string, index int, value string) error {
	s, err := c.p.PickClient(key)
	if err != nil {
		return err
	}
	return s.LSet(key, index, value)
}

// ItemCount returns an amount of items in all storages
func (c *Client) ItemCount() (int, error) {
	ic := 0
	for _, v := range c.p.GetAll() {
		count, err := v.ItemCount()
		if err != nil {
			return 0, err
		}
		ic += count
	}
	return ic, nil
}

// Keys returns all keys in storage
func (c *Client) Keys() ([]string, error) {
	var keys []string
	for _, v := range c.p.GetAll() {
		skeys, err := v.Keys()
		if err != nil {
			return nil, err
		}
		keys = append(keys, skeys...)
	}
	return keys, nil
}
