// Package reader is a helper package to read TELNET lines
package reader

import (
	"bytes"
	"github.com/reiver/go-telnet"
)

// Reader is an interface to different reader implementations
type Reader interface {
	ReadLine() ([]byte, error)
}

// TelnetReader is a helper to read TELNET lines from Reader interface
type TelnetReader struct {
	r telnet.Reader
}

// NewTelnetReader initializes TelnetReader struct
func NewTelnetReader(r telnet.Reader) *TelnetReader {
	return &TelnetReader{r: r}
}

// ReadLine read until telnet line delimiter, which is CRLF
func (r *TelnetReader) ReadLine() ([]byte, error) {
	var cmdBuf []byte
	var buffer [1]byte
	p := buffer[:]

	// Reading one byte
	for {
		n, err := r.r.Read(p)

		switch {
		case n <= 0 && err == nil:
			continue
		case n <= 0 && err != nil:
			return nil, err
		case p[0] != '\n':
			cmdBuf = append(cmdBuf, p[0])
			continue
		// If we receive line ending, but without caret return
		// We are still filling buffer as it might be a part of string to store
		case p[0] == '\n' && cmdBuf[len(cmdBuf)-1] != '\r':
			cmdBuf = append(cmdBuf, p[0])
			continue
		case p[0] == '\n' && cmdBuf[len(cmdBuf)-1] != '\r':
			cmdBuf = append(cmdBuf, p[0])
			continue
		// That is remove character
		case p[0] == '\b':
			cmdBuf = cmdBuf[:len(cmdBuf)-1]
			continue
		}

		return bytes.TrimRight(cmdBuf, "\r\n"), nil
	}
}
