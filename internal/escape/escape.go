package escape

import "strings"

// Whitespaces checks if string contains any if escaped chars and wraps it with doublequotes
func Whitespace(s string) string {
	if strings.ContainsAny(s, " \n") {
		return `"` + s + `"`
	}

	return s
}

// ParseString is used to parse string into pieces following escape rule
func ParseString(s string) []string {
	var args []string
	var buf string

	var isEscaped bool

	for _, c := range s {
		switch {
		case c == '"':
			isEscaped = !isEscaped
		case c == ' ' && !isEscaped:
			args = append(args, buf)
			buf = ""
		default:
			buf += string(c)
		}
	}

	return append(args, buf)
}
