package escape

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func Test_ParseString(t *testing.T) {
	res := ParseString(`SET test 0 list 1 2 "3 4" 5`)
	exp := []string{"SET", "test", "0", "list", "1", "2", "3 4", "5"}
	assert.Equal(t, exp, res)
}
