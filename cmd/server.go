package main

import (
	"bitbucket.org/pvasilenka/juno-test/db"
	"bitbucket.org/pvasilenka/juno-test/server"
	"bitbucket.org/pvasilenka/juno-test/server/handler"
	"context"
	"flag"
	"github.com/pkg/errors"
	"github.com/reiver/go-telnet"
	"log"
	"os"
	"os/signal"
	"syscall"
)

var (
	port        = flag.String("port", "5001", "Port for server to listen")
	authEnabled = flag.Bool("enable-auth", false, "Defines whether enable or not authentication. If true, user and password should be passed")
	authUser    = flag.String("auth-user", "", "Username to authenticate")
	authPwd     = flag.String("auth-pwd", "", "Password to authenticate")
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		ch := make(chan os.Signal)
		signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
		<-ch
		cancel()
		return
	}()

	if err := validateFlags(); err != nil {
		log.Fatal(err)
	}

	InitApplication(ctx)
	<-ctx.Done()
}

func validateFlags() error {
	if *authEnabled && *authUser == "" && *authPwd == "" {
		return errors.New("if auth enabled, auth-user and auth-pwd should be provided and not empty")
	}

	return nil
}

// InitApplication initializes and configures application
func InitApplication(ctx context.Context) *server.Application {
	st := db.NewStorage(ctx)
	h := handler.NewHandler(ctx)
	ts := &telnet.Server{Handler: h, Addr: ":" + *port}
	app := server.NewApplication(st, ts)

	h.AddHandler("GET", app.Get)
	h.AddHandler("SET", app.Set)
	h.AddHandler("UPDATE", app.Update)
	h.AddHandler("DELETE", app.Delete)
	h.AddHandler("KEYS", app.Keys)
	h.AddHandler("COUNT", app.Count)
	h.AddHandler("DGETK", app.GetElementFromDictByKey)
	h.AddHandler("DSETK", app.SetElementToDictByKey)
	h.AddHandler("LGET", app.GetElementFromListByIndex)
	h.AddHandler("LSET", app.SetElementToListByIndex)

	if *authEnabled {
		h.EnableAuth(*authUser, *authPwd)
	}

	// Current version of TELNET library doesn't support context
	// So in case of signal catch it doesn't stop TELNET server
	// So we run it in a separate goroutine and it closes when program is over
	go func() {
		log.Println("Starting TELNET server on port", *port)
		if err := app.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()

	return app
}
