package main

import (
	"bitbucket.org/pvasilenka/juno-test/client"
	"bitbucket.org/pvasilenka/juno-test/client/caller"
	"bitbucket.org/pvasilenka/juno-test/db/node"
	"context"
	"github.com/reiver/go-telnet"
	"log"
	"time"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	cler1 := caller.NewClientCaller(ctx, &caller.AuthConfig{Username: "pavel", Password: "123"})
	defer cler1.Close()

	cler2 := caller.NewClientCaller(ctx, &caller.AuthConfig{Username: "pavel", Password: "123"})
	defer cler2.Close()

	c := client.NewClient(cler1, cler2)

	// Starting telnet client in a separate goroutine
	go telnet.DialToAndCall("127.0.0.1:5000", cler1)
	go telnet.DialToAndCall("127.0.0.1:5001", cler2)

	if err := c.Set("test", node.NewStringNode("test2", 0)); err != nil {
		log.Println("[ERROR]", err)
	}

	n, err := c.Get("test")
	if err != nil {
		log.Println("[ERROR]", err)
	} else {
		log.Println(n.String())
	}

	n, err = c.Get("test2")
	if err != nil {
		log.Println("[ERROR]", err)
	} else {
		log.Println(n.String())
	}

	n, err = c.Get("test3")
	if err != nil {
		log.Println("[ERROR]", err)
	} else {
		log.Println(n.String())
	}

	err = c.Set("test4", node.NewListNode([]string{"val1", "val2"}, 0))
	if err != nil {
		log.Println("[ERROR]", err)
	}

	n, err = c.Get("test4")
	if err != nil {
		log.Println("[ERROR]", err)
	} else {
		log.Println(n.String())
	}

	n, err = c.LGet("test4", 0)
	if err != nil {
		log.Println("[ERROR]", err)
	} else {
		log.Println(n.String())
	}

	err = c.LSet("test4", 0, "yee")
	if err != nil {
		log.Println("[ERROR]", err)
	}

	n, err = c.Get("test4")
	if err != nil {
		log.Println("[ERROR]", err)
	} else {
		log.Println(n.String())
	}

	err = c.Set("test5", node.NewDictNode(map[string]string{"val1": "val2"}, 0))
	if err != nil {
		log.Println("[ERROR]", err)
	}

	n, err = c.Get("test5")
	if err != nil {
		log.Println("[ERROR]", err)
	} else {
		log.Println(n.String())
	}

	n, err = c.DGetK("test5", "val1")
	if err != nil {
		log.Println("[ERROR]", err)
	} else {
		log.Println(n.String())
	}

	err = c.DSetK("test5", "val2", "yeee")
	if err != nil {
		log.Println("[ERROR]", err)
	}

	n, err = c.Get("test5")
	if err != nil {
		log.Println("[ERROR]", err)
	} else {
		log.Println(n.String())
	}

	err = c.Set("test6", node.NewDictNode(map[string]string{"val1": "val2"}, 1*time.Second))
	if err != nil {
		log.Println("[ERROR]", err)
	}
	n, err = c.Get("test6")
	if err != nil {
		log.Println("[ERROR]", err)
	} else {
		log.Println(n.String())
	}

	time.Sleep(1 * time.Second)
	n, err = c.Get("test6")
	if err != nil {
		log.Println("[ERROR]", err)
	} else {
		log.Println(n.String())
	}

	err = c.Set("test6", node.NewDictNode(map[string]string{"val1 val2": "val2 val3"}, 0))
	if err != nil {
		log.Println("[ERROR]", err)
	}

	n, err = c.Get("test6")
	if err != nil {
		log.Println("[ERROR]", err)
	} else {
		log.Println(n.String())
	}

	cancel()
	<-ctx.Done()
}
