package command

import "errors"

// DGetK is a command to fetch item from dict by key
type DGetK struct {
	*Base
}

// Validate validates command
func (s *DGetK) Validate() error {
	if len(s.Args) != 2 {
		return errors.New("cmd format is <CMD> <key> <dict_key>")
	}

	return nil
}

// GetKey returns key passed to args list
func (s *DGetK) GetKey() string {
	return s.Args[0]
}

// GetDictKey returns dict key
func (s *DGetK) GetDictKey() string {
	return s.Args[1]
}
