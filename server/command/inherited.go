package command

// DeleteCommand represents delete feature
type DeleteCommand struct {
	*GetCommand
}

// KeysCommand represents keys feature
type KeysCommand struct {
	*Base
}

// UpdateCommand represents update feature
type UpdateCommand struct {
	*SetCommand
}
