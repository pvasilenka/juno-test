package command

import (
	"errors"
)

// AuthCommand represents set feature
type AuthCommand struct {
	*Base
}

// Validate validates command
func (s *AuthCommand) Validate() error {
	if len(s.Args) != 2 {
		return errors.New("cmd format is AUTH <username> <password>")
	}

	return nil
}

// GetUsername returns username from args list
func (s *AuthCommand) GetUsername() string {
	return s.Args[0]
}

// GetPassword returns password from args list
func (s *AuthCommand) GetPassword() string {
	return s.Args[1]
}
