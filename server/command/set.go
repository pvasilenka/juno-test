package command

import (
	"errors"
	"strconv"
	"time"
)

const week = 1 * 60 * 60 * 24 * 7 // seconds

// SetCommand represents set feature
type SetCommand struct {
	*Base

	ttl int
}

// Validate validates command
func (s *SetCommand) Validate() error {
	if len(s.Args) < 4 {
		return errors.New("cmd format is <CMD> <key> <ttl> <type> <value>")
	}

	ttl, err := strconv.Atoi(s.Args[1])
	if err != nil {
		return errors.New("invalid value passed to ttl field")
	}

	s.ttl = ttl

	if ttl < 0 || ttl > week {
		return errors.New("ttl should be more then zero and less than a week in seconds")
	}

	return nil
}

// GetTTL returns TTL. Zero TTL means item will not expire
func (s *SetCommand) GetTTL() time.Duration {
	return time.Duration(s.ttl) * time.Second
}

// GetKey return key in storage
func (s *SetCommand) GetKey() string {
	return s.Args[0]
}

// GetType returns item type.
func (s *SetCommand) GetType() string {
	return s.Args[2]
}

// GetValue returns string representation of item
// Should be later converted based on Type
func (s *SetCommand) GetValue() []string {
	return s.Args[3:]
}
