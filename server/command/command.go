package command

import (
	"strings"
	"time"
)

// Command is base interface for command
type Command interface {
	GetName() string
}

type WithData interface {
	GetType() string
	GetValue() []string
	GetTTL() time.Duration
}

// Base represents command which come out from
type Base struct {
	Name string
	Args []string // We need args for tuple-like interaction
}

// GetName returns name of command
func (cb *Base) GetName() string {
	return strings.ToUpper(cb.Name)
}
