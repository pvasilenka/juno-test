package command

import (
	"errors"
)

// GetCommand represents get feature
type GetCommand struct {
	*Base
}

// Validate validates command
func (s *GetCommand) Validate() error {
	if len(s.Args) != 1 {
		return errors.New("cmd format is <CMD> <key>")
	}

	return nil
}

// GetKey returns key passed to args list
func (s *GetCommand) GetKey() string {
	return s.Args[0]
}
