package command

import "errors"

// DGetK is a command to fetch item from dict by key
type DSetK struct {
	*Base
}

// Validate validates command
func (s *DSetK) Validate() error {
	if len(s.Args) != 3 {
		return errors.New("cmd format is <CMD> <key> <dict_key> <string_value>")
	}

	return nil
}

// GetKey returns key passed to args list
func (s *DSetK) GetKey() string {
	return s.Args[0]
}

// GetDictKey returns dict key
func (s *DSetK) GetDictKey() string {
	return s.Args[1]
}

// GetItem returns value to set
func (s *DSetK) GetValue() string {
	return s.Args[2]
}
