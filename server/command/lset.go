package command

import (
	"errors"
	"strconv"
)

// LGet is a command to get item from stored list by index
type LSet struct {
	*Base

	index int
}

// Validate validates command
func (s *LSet) Validate() error {
	if len(s.Args) != 3 {
		return errors.New("cmd format is <CMD> <key> <index> <string_value>")
	}

	ind, err := strconv.Atoi(s.Args[1])
	if err != nil {
		return err
	}

	if ind < 0 {
		return errors.New("index should be more than zero")
	}

	s.index = ind

	return nil
}

// GetKey returns key passed to args list
func (s *LSet) GetKey() string {
	return s.Args[0]
}

// GetIndex index of item to be fetched
func (s *LSet) GetIndex() int {
	return s.index
}

// GetValue value of item to store
func (s *LSet) GetValue() string {
	return s.Args[2]
}
