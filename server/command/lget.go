package command

import (
	"errors"
	"strconv"
)

// LGet is a command to get item from stored list by index
type LGet struct {
	*Base

	index int
}

// Validate validates command
func (s *LGet) Validate() error {
	if len(s.Args) != 2 {
		return errors.New("cmd format is <CMD> <key> <index>")
	}

	ind, err := strconv.Atoi(s.Args[1])
	if err != nil {
		return err
	}

	if ind < 0 {
		return errors.New("index should be more than zero")
	}

	s.index = ind

	return nil
}

// GetKey returns key passed to args list
func (s *LGet) GetKey() string {
	return s.Args[0]
}

// GetIndex index of item to be fetched
func (s *LGet) GetIndex() int {
	return s.index
}
