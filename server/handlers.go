package server

import (
	"bitbucket.org/pvasilenka/juno-test/db/node"
	"bitbucket.org/pvasilenka/juno-test/server/command"
	"strconv"
	"strings"
)

// Get command gets data from storage
func (s *Application) Get(c command.Command) (string, error) {
	cm := &command.GetCommand{Base: c.(*command.Base)}
	if err := cm.Validate(); err != nil {
		return "", err
	}

	n, err := s.db.Get(cm.GetKey())

	if err != nil {
		return "", err
	}

	return n.String(), nil
}

// Set command sets data to storage
func (s *Application) Set(c command.Command) (string, error) {
	cm := &command.SetCommand{Base: c.(*command.Base)}

	if err := cm.Validate(); err != nil {
		return "", err
	}

	n, err := s.getNode(cm)
	if err != nil {
		return "", err
	}
	s.db.Set(cm.GetKey(), n)

	return "", nil
}

// Updates update data by key. Will reset row TTL
func (s *Application) Update(c command.Command) (string, error) {
	cm := &command.UpdateCommand{
		SetCommand: &command.SetCommand{Base: c.(*command.Base)},
	}

	if err := cm.Validate(); err != nil {
		return "", err
	}

	n, err := s.getNode(cm)
	if err != nil {
		return "", err
	}

	return "", s.db.Update(cm.GetKey(), n)
}

// Delete removes data from storage
func (s *Application) Delete(c command.Command) (string, error) {
	cm := &command.DeleteCommand{
		GetCommand: &command.GetCommand{Base: c.(*command.Base)},
	}

	if err := cm.Validate(); err != nil {
		return "", err
	}

	return "", s.db.Delete(cm.GetKey())
}

// GetElementFromDictByKey fetches element from dict by key
func (s *Application) GetElementFromDictByKey(c command.Command) (string, error) {
	cm := &command.DGetK{Base: c.(*command.Base)}

	if err := cm.Validate(); err != nil {
		return "", err
	}

	resp, err := s.db.GetElementFromDictByKey(cm.GetKey(), cm.GetDictKey())
	if err != nil {
		return "", err
	}

	return resp.String(), nil
}

// SetElementToDictByKey puts element to dict by key
func (s *Application) SetElementToDictByKey(c command.Command) (string, error) {
	cm := &command.DSetK{Base: c.(*command.Base)}

	if err := cm.Validate(); err != nil {
		return "", err
	}

	return "", s.db.SetElementToDictByKey(cm.GetKey(), cm.GetDictKey(), cm.GetValue())
}

// GetElementFromListByIndex returns element from list by index
func (s *Application) GetElementFromListByIndex(c command.Command) (string, error) {
	cm := &command.LGet{Base: c.(*command.Base)}

	if err := cm.Validate(); err != nil {
		return "", err
	}

	resp, err := s.db.GetElementFromListByIndex(cm.GetKey(), cm.GetIndex())
	if err != nil {
		return "", err
	}

	return resp.String(), nil
}

// SetElementToListByIndex puts element to list by index
func (s *Application) SetElementToListByIndex(c command.Command) (string, error) {
	cm := &command.LSet{Base: c.(*command.Base)}

	if err := cm.Validate(); err != nil {
		return "", err
	}

	return "", s.db.SetElementToListByIndex(cm.GetKey(), cm.GetIndex(), cm.GetValue())
}

// Keys will return a list of keys stored in storage
func (s *Application) Keys(c command.Command) (string, error) {
	return strings.Join(s.db.Keys(), " "), nil
}

// Count will return amount of values stored in storage
func (s *Application) Count(c command.Command) (string, error) {
	return strconv.Itoa(s.db.ItemCount()), nil
}

func (s *Application) getNode(c command.WithData) (node.Node, error) {
	return node.GetNodeFromString(c.GetType(), c.GetValue(), c.GetTTL())
}
