package server

import (
	"bitbucket.org/pvasilenka/juno-test/db"
	"github.com/reiver/go-telnet"
)

// ListenAndServer requires struct to provide listen and serve feature
type ListenAndServer interface {
	ListenAndServe() error
}

// Application is a server application
// which handles all bypassing from server to inmemory storage
type Application struct {
	Server ListenAndServer

	db *db.Storage
	h  telnet.Handler
}

// NewApplication returns instance of application
func NewApplication(db *db.Storage, ts *telnet.Server) *Application {
	return &Application{
		Server: ts,
		db:     db,
	}
}

// ListenAndServe is an adaptor for Telnet server
func (s *Application) ListenAndServe() error {
	return s.Server.ListenAndServe()
}
