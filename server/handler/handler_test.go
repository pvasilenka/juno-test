package handler

import (
	"bitbucket.org/pvasilenka/juno-test/server/command"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Benchmark_Parse(b *testing.B) {
	h := &Handler{}
	body := []byte("SET test list 1 2 3 4 5\r\n")
	for i := 0; i < b.N; i++ {
		h.parse(body)
	}
}

func Test_Parse(t *testing.T) {
	h := &Handler{}
	body := []byte("SET test list 1 2 3 4 5\r\n")

	cmd, err := h.parse(body)
	if err != nil {
		t.Error(err)
	}

	exp := &command.Base{
		Name: "SET",
		Args: []string{"test", "list", "1", "2", "3", "4", "5"},
	}

	assert.Equal(t, exp, cmd)
}

func Test_Parse_Error(t *testing.T) {
	h := &Handler{}
	_, err := h.parse([]byte{})
	assert.EqualError(t, err, ErrWrongFormatted.Error())
}

func Test_ParseEscaped_Error(t *testing.T) {
	h := &Handler{}
	body := []byte(`SET test list "1 2 3 4 5`)
	_, err := h.parse(body)
	assert.EqualError(t, err, ErrWrongFormatted.Error())
}

func Test_ParseEscaped(t *testing.T) {
	h := &Handler{}
	body := []byte(`SET test list "1 2 3 4 5"`)
	body = append(body, '\r', '\n')

	cmd, err := h.parse(body)
	if err != nil {
		t.Error(err)
	}

	exp := &command.Base{
		Name: "SET",
		Args: []string{"test", "list", "1 2 3 4 5"},
	}

	assert.Equal(t, exp, cmd)

	_, err = h.parse([]byte{})
	assert.EqualError(t, err, ErrWrongFormatted.Error())
}
