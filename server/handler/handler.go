package handler

import (
	"bitbucket.org/pvasilenka/juno-test/internal/reader"
	"bitbucket.org/pvasilenka/juno-test/server/command"
	"context"
	"errors"
	"github.com/reiver/go-oi"
	"github.com/reiver/go-telnet"
	"log"
)

// Internal response messages
const (
	GreetingMessage = "HELLO\r\n"
	ByeMessage      = "BYE\r\n"
)

// Internal commands
const (
	CommandQuit = "QUIT"
	CommandAuth = "AUTH"
)

// Server errors
var (
	ErrUnknownCommand   = errors.New("unknown command")
	ErrWrongFormatted   = errors.New("wrong formatted message")
	ErrWrongCredentials = errors.New("wrong credentials")
	ErrAuthRequired     = errors.New("auth required")
)

// HandlerFunc is a func which handles command passed to server
type HandlerFunc func(c command.Command) (string, error)

// Handler is a TELNET server handler which accepts bytes from stream, parses that in command
// and bypassed it to defined handle
type Handler struct {
	h   map[string]HandlerFunc
	ctx context.Context

	authEnabled bool
	un          string
	pwd         string
}

// NewHandler returns handler instance
func NewHandler(ctx context.Context) *Handler {
	return &Handler{
		ctx: ctx,
		h:   make(map[string]HandlerFunc),
	}
}

// AddHandler is used to add new handler to command
func (h *Handler) AddHandler(command string, hndl HandlerFunc) {
	h.h[command] = hndl
}

// EnableAuth enables server plain auth by user and password
func (h *Handler) EnableAuth(user, password string) {
	h.authEnabled = true
	h.un = user
	h.pwd = password
}

// ServeTELNET is a function which serves one incoming TELNET connection
func (h *Handler) ServeTELNET(ctx telnet.Context, w telnet.Writer, r telnet.Reader) {
	authorized := false

	oi.LongWriteString(w, GreetingMessage)
	tr := reader.NewTelnetReader(r)

	for {
		select {
		case <-h.ctx.Done():
			return
		default:
			cmdBuf, err := tr.ReadLine()
			if err != nil {
				log.Println(err)
				return
			}

			cmd, err := h.parse(cmdBuf)
			if err != nil {
				h.writeError(w, err)
				continue
			}

			// internal quit command
			if cmd.GetName() == CommandQuit {
				oi.LongWriteString(w, ByeMessage)
				return
			}

			// internal auth command
			if cmd.GetName() == CommandAuth {
				// if auth disabled handler should always say user is authenticated with any username and password
				if !h.authEnabled {
					h.writeOK(w, "")
					continue
				}

				authCmd := command.AuthCommand{Base: cmd}
				if err := authCmd.Validate(); err != nil {
					h.writeError(w, err)
					continue
				}

				if err := h.authenticate(authCmd.GetUsername(), authCmd.GetPassword()); err != nil {
					h.writeError(w, err)
					continue
				}

				authorized = true
				h.writeOK(w, "")
				continue
			}

			if !authorized && h.authEnabled {
				h.writeError(w, ErrAuthRequired)
				continue
			}

			hl, err := h.getHandler(cmd)
			if err != nil {
				h.writeError(w, err)
				continue
			}

			resp, err := hl(cmd)
			if err != nil {
				h.writeError(w, err)
				continue
			}

			h.writeOK(w, resp)
		}
	}
}

// authenticate authenticates by user and password
func (h *Handler) authenticate(un, pwd string) error {
	if h.un != un || h.pwd != pwd {
		return ErrWrongCredentials
	}

	return nil
}

func (h *Handler) writeError(w telnet.Writer, err error) {
	oi.LongWriteString(w, "ERROR\r\n"+err.Error()+"\r\n")
}

func (h *Handler) writeOK(w telnet.Writer, resp string) {
	oi.LongWriteString(w, "OK\r\n")
	if resp != "" {
		oi.LongWriteString(w, resp+"\r\n")
	}
}

// parse parses byte body into command struct
func (h *Handler) parse(b []byte) (*command.Base, error) {
	var buf []byte
	var args []string

	// says if double quotes are opened
	isEscaped := false

	for _, v := range b {
		switch {
		case v == '"':
			isEscaped = !isEscaped
		case v == ' ' && !isEscaped:
			args = append(args, string(buf))
			buf = buf[:0]
		// Skipping caret return and new line if it isn't escaped
		case v == '\r', v == '\n' && !isEscaped:
		default:
			buf = append(buf, v)
		}
	}

	// Quote should be closed
	if isEscaped {
		return nil, ErrWrongFormatted
	}

	args = append(args, string(buf))
	if len(args) == 0 || args[0] == "" {
		return nil, ErrWrongFormatted
	}

	return &command.Base{
		Name: args[0],
		Args: args[1:],
	}, nil
}

func (h *Handler) getHandler(c command.Command) (HandlerFunc, error) {
	if h, ok := h.h[c.GetName()]; !ok {
		return nil, ErrUnknownCommand
	} else {
		return h, nil
	}
}
