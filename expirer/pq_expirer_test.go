package expirer

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestPQExpirer_Expire(t *testing.T) {
	pqe := NewPQExpirer(context.Background())
	start := time.Now()

	order := []string{
		"test7",
		"test3",
		"test6",
		"test5",
		"test4",
		"test2",
		"test1",
		"test8",
		"test9",
	}

	type test = struct {
		key string
		ttl time.Time
	}

	keys := []*test{
		{"test1", start.Add(7 * time.Second)},
		{"test2", start.Add(6 * time.Second)},
		{"test3", start.Add(2 * time.Second)},
		{"test4", start.Add(5 * time.Second)},
		{"test5", start.Add(4 * time.Second)},
		{"test6", start.Add(3 * time.Second)},
		{"test7", start.Add(1 * time.Second)},
		{"test8", start.Add(8 * time.Second)},
		{"test9", start.Add(9 * time.Second)},
	}

	go func() {
		for _, v := range keys {
			pqe.Expire(v.key, v.ttl)
		}
	}()

	i := 0
	for v := range pqe.Expired() {
		assert.Equal(t, order[i], v.GetKey())
		fmt.Println("item", v.GetKey(), "expired during", time.Since(start).String())
		i++

		if len(order) == i {
			break
		}
	}
}
