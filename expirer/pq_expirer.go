package expirer

import (
	"container/heap"
	"context"
	"time"
)

// PQExpirer is an expirer which uses priority queue for expiring
// Priority Queue is used to limit number of goroutines and improve performance and memory consumption
type PQExpirer struct {
	pq      *priorityQueue
	e       chan Expired
	newItem chan *item
	ctx     context.Context
}

// NewPQExpirer initializes expirer and starting service goroutine
func NewPQExpirer(ctx context.Context) *PQExpirer {
	pq := &priorityQueue{}
	heap.Init(pq)

	pqe := &PQExpirer{
		e:       make(chan Expired),
		newItem: make(chan *item),
		pq:      pq,
		ctx:     ctx,
	}

	go pqe.service()

	return pqe
}

// Expire is a function which accepts key to service and time when to service
// It also notifies expiring goroutine we have new item
func (pqe *PQExpirer) Expire(key string, expiredAt time.Time) {
	// That should be run in a separate goroutine not to affect external operations
	go func() {
		pqe.newItem <- &item{
			key:      key,
			expireAt: expiredAt,
		}
	}()
}

// service starts long-running process to serve values using priority queue
func (pqe *PQExpirer) service() {
	for {
		var sleepTime time.Duration
		// If any items in the queue
		if pqe.pq.Len() > 0 {
			// Sleeping until time of next expiring item
			sleepTime = pqe.pq.items[0].expireAt.Sub(time.Now())
		} else {
			// Otherwise sleep for one hour
			sleepTime = 1 * time.Hour
		}

		t := time.NewTimer(sleepTime)

		select {
		case <-t.C:
			// If no items in queue sleeping for one more hour
			if pqe.pq.Len() == 0 {
				continue
			}

			// if the first item in the loop is expired we're saying it is and removing from heap
			if pqe.pq.items[0].expired() {
				pqe.e <- pqe.pq.items[0]
				pqe.pq.remove(pqe.pq.items[0])
			}
		// If new item received we should restart this loop as it might expire before time sleep will finish
		case i := <-pqe.newItem:
			pqe.pq.push(i)
			// Stopping timer to allow GC to remove it from memory before it finishes
			t.Stop()
			continue
		case <-pqe.ctx.Done():
			return
		}
	}
}

// Expired returns channel of expired values
func (pqe *PQExpirer) Expired() <-chan Expired {
	return pqe.e
}

type item struct {
	key        string
	expireAt   time.Time
	queueIndex int
}

// GetKey returns expired key
func (i *item) GetKey() string {
	return i.key
}

func (i *item) expired() bool {
	return i.expireAt.Before(time.Now())
}

type priorityQueue struct {
	items []*item
}

func (pq *priorityQueue) push(i *item) {
	heap.Push(pq, i)
}

func (pq *priorityQueue) remove(i *item) {
	heap.Remove(pq, i.queueIndex)
}

// Heap interface implementation
func (pq *priorityQueue) Less(i, j int) bool {
	return pq.items[i].expireAt.Before(pq.items[j].expireAt)
}

func (pq *priorityQueue) Swap(i, j int) {
	pq.items[i], pq.items[j] = pq.items[j], pq.items[i]
	pq.items[i].queueIndex = i
	pq.items[j].queueIndex = j
}

func (pq *priorityQueue) Push(x interface{}) {
	item := x.(*item)
	item.queueIndex = len(pq.items)
	pq.items = append(pq.items, item)
}

func (pq *priorityQueue) Pop() interface{} {
	old := pq.items
	n := len(old)
	item := old[n-1]
	item.queueIndex = -1
	pq.items = old[0 : n-1]
	return item
}

func (pq *priorityQueue) Len() int {
	return len(pq.items)
}
