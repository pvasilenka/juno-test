package expirer

import "time"

// Expired is an interface for expired value
type Expired interface {
	GetKey() string
}

// KeyExpirer is an interface to service keys at some time
type KeyExpirer interface {
	Expire(key string, expiredAt time.Time)
	Expired() <-chan Expired
}
