# In Memory Cache

## Main Features:

- Key-value storage with string, lists, dict support
- Per-key TTL
- Operations:
    - Get
    - Set
    - Update
    - Remove
    - Keys
    - Custom operations(Get i element on list, get value by key from dict, etc)
- Golang API client
- Telnet-like/HTTP-like API protocol
- Provide some tests, API spec, deployment docs without full coverage, just a few cases and some examples of telnet/http calls to the server.

## Protocol Description

Implemented text-based TELNET protocol.

### Responses

* `OK` - is sent when everything is OK. If command requires some output, server will send it nextline.
* `ERROR`- is sent when some error occurred. Always followed with error message on the next line.
 
### Commands

* `QUIT`
    * Command `QUIT` closes connection and exits from server.
    * Response is always `BYE`
* `AUTH`
    * `AUTH <username> <password>` - authenticates on server using plain authentication
    * If authenticated, response would be just OK
    * Otherwise, response would be `wrong credentials`
* `SET <key> <ttl> <type> <value>` - sets value to storage  
* `GET <key>` - returns value by key
* `UPDATE <key> <value>` - updates by key and value, resetting TTL
* `DELETE <key>` - removes item from server
* `DGETK <key> <dkey>` - fetches item from dict by key and dkey
* `DSETK <key> <dkey> <string_value>` - set item to dict by key and dkey
* `LGET <key> <index>` - fetches item from list by key and index
* `LSET <key> <index> <string_value>` - set item to list by key and index

### Value Formatting

When some value is sent, it's type followed by value string representation

* `string`
    * For string just plain text
* `list`
    * For list items are delimited with spacebar, i.e. `item1 item2 item3 item4`
* `dict`
    * For dicts key value pairs are spacebar delimited, i.e. `key1 value1 key2 value2 key3 value3`

### Escaping

Protocol allows us to escape few values, such as whitespace and/or line delimiter (only `\n`).
To escape, use doublequote symbols `"`.

Example:

    > SET test 0 list 1 2 3 4 5
    // Will set to database list ['1', '2', '3', '4', '5']
    > SET test 0 list "1 2 3 4 5"
    // Will set to database list ['1 2 3 4 5']

### Client-Side scaling

That is possible to make client-side scaling.
It uses memcahched-like server using consistent hashing algorithm

	ctx, cancel := context.WithCancel(context.Background())

	cler1 := caller.NewClientCaller(ctx, &caller.AuthConfig{Username: "pavel", Password: "123"})
	defer cler1.Close()

	cler2 := caller.NewClientCaller(ctx, &caller.AuthConfig{Username: "pavel", Password: "123"})
	defer cler2.Close()

	c := client.NewClient(cler1, cler2)

	// Starting telnet client in a separate goroutine
	go telnet.DialToAndCall("127.0.0.1:5000", cler1)
	go telnet.DialToAndCall("127.0.0.1:5001", cler2)

## To Do:

- persistence to disk/db
- perfomance tests (using Golang Benchmarks)